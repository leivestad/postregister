package no.ntnu.idatg2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Maven test does not have access, but it works in intelliJ
 */
class PostalCodeTest {


    /**
     * Tests the constructor
     */
    @Test
    public void testPostalCodeTest()
    {
        System.out.println("Testing Postal Code class");
        PostalCode postalCode = new PostalCode("5899", "Eikefjord", "2234", "Sunnfjord", "P");
        assertEquals("5899", postalCode.getPostalCode());
        assertEquals("Eikefjord", postalCode.getCity());
        assertEquals("2234", postalCode.getMunicipalCode());
        assertEquals("Sunnfjord", postalCode.getMunicipal());
        assertEquals("P", postalCode.getPostalCategory());

    }



}