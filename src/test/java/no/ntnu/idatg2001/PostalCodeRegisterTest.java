package no.ntnu.idatg2001;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Maven test does not have access, but it works in intelliJ
 */
class PostalCodeRegisterTest {

    /**
     * Tests that you can put PostalCodes object in the arrayList.
     */
    @Test
    void addPostalCode() {
        ArrayList<PostalCode> postalCodeRegister = new ArrayList<>();

        assertEquals(new ArrayList<>(), postalCodeRegister);
        assertNotEquals(4, postalCodeRegister.size());

        postalCodeRegister.add(new PostalCode("6900", "Florø", "4000", "Kinn", "G"));
        postalCodeRegister.add(new PostalCode("4646", "Naustdal", "5000", "Sunnfjord", "F"));
        postalCodeRegister.add(new PostalCode("7979", "Nordfjordeid", "8000", "Stadt","A"));
        postalCodeRegister.add(new PostalCode("2815", "Gjøvik", "6000", "Gjøvik", "B"));

        assertEquals(4, postalCodeRegister.size());

        postalCodeRegister.remove(1);
        assertEquals(3, postalCodeRegister.size());
    }

}