package no.ntnu.idatg2001;

import java.util.ArrayList;
import java.util.List;

/**
 * PostalCodeRegister class
 * holds the instances of Postal Codes
 * @author Niklas Leivestad
 */

public class PostalCodeRegister {

    /**
     * The collection of PostalCode instances.
     */
    private final ArrayList<PostalCode> PostalCodeRegister;

    /**
     * Creates an instance of the postal code register.
     */
    public PostalCodeRegister() {
        this.PostalCodeRegister = new ArrayList<>();
    }

    /**
     * Returns the list of postal codes.
     *
     * @return the list of PostalCode.
     */
    public List<PostalCode> getPostalList() {
        return this.PostalCodeRegister;
    }

    /**
     * Add a PostalCode instance to the register.
     *
     * @param patient patient object
     */
    public void addPostalCode(PostalCode postalCode) {
        this.PostalCodeRegister.add(postalCode);
    }

    /**
     * Removes the postalCode provided by the parameter, from the register.
     * Returns <code>true</code> is remove was successful.
     *
     * @param postalCode the patient object to remove
     * @return <code>true</code> is remove was successful
     */
    public boolean remove(PostalCode postalCode) {
        return this.PostalCodeRegister.remove(postalCode);
    }

}
