package no.ntnu.idatg2001;
/**
 * @author Niklas Leivestad
 *
 */
public class PostalCode {
    String postalCode;
    String city;
    String municipalCode; // = String.format((county 2 + municipality 2));
    String municipal;
    String postalCategory;




    /**
     * Constructor
     *
     * @param postalCode     first name of patient
     * @param city           city name
     * @param municipalCode  municipal code
     * @param municipal      municipal
     * @param postalCategory postalCategory
     */

    public PostalCode(String postalCode, String city, String municipalCode, String municipal, String postalCategory) {
        this.postalCode = postalCode;
        this.city = city;
        this.municipalCode = municipalCode;
        this.municipal = municipal;
        this.postalCategory = postalCategory;
    }


    /**
     * @return all variables for testing with the smaller postcodes.txt
     */
    @Override
    public String toString() {
        return "PostalCode{" + postalCode + " " + city + " " + municipalCode + " " + municipal + " " +  postalCategory + '\'' + '}';
    }

    /**
     * Getters
     */
    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getMunicipalCode() {
        return municipalCode;
    }

    public String getMunicipal() {
        return municipal;
    }

    public String getPostalCategory() {
        return postalCategory;
    }

    /**
     * Setters
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setMunicipalCode(String municipalCode) {
        this.municipalCode = municipalCode;
    }

    public void setMunicipal(String municipal) {
        this.municipal = municipal;
    }

    public void setPostalCategory(String postalCategory) {
        this.postalCategory = postalCategory;
    }
}

