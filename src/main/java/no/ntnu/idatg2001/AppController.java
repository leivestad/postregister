package no.ntnu.idatg2001;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.net.URL;

/**
 * AppController class
 * Where the fx meets the Java code
 * Here we control what is made in fxml
 * @author Niklas Leivestad
 */
public class AppController implements Initializable {


    //--------------------------Tableview------------------------------------------------------------------------

    /**
     * The Oblist containing all data for the Tableview
     * searchOblist for adding the matching ones from user input
     */
    ObservableList<PostalCode> oblist = FXCollections.observableArrayList();
    ObservableList<PostalCode> searchOblist = FXCollections.observableArrayList();

    private PostalCodeRegister postalCodeRegister;

    @FXML
    private TableView<PostalCode> postalCodeTableView;

    @FXML
    private javafx.scene.control.Button closeButton;

    @FXML
    private TableColumn<PostalCode, String>
            postalCodeColoumn,
            cityColoumn,
            municipalCodeColoumn,
            municipalColoumn,
            postalCategoryColoumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Create the business logic by creating an instance of
        // PostalCodeRegister and filling it with dummy data.
        this.postalCodeRegister = new PostalCodeRegister();
        //this.fillRegisterWithDummyData();
        this.readFile();

        // Finalize the setup of the TableView
        postalCodeColoumn.setMinWidth(100);
        postalCodeColoumn.setCellValueFactory(new PropertyValueFactory<>("postalCode"));

        cityColoumn.setMinWidth(140);
        cityColoumn.setCellValueFactory(new PropertyValueFactory<>("city"));

        municipalCodeColoumn.setMinWidth(100);
        municipalCodeColoumn.setCellValueFactory(new PropertyValueFactory<>("municipalCode"));

        municipalColoumn.setMinWidth(200);
        municipalColoumn.setCellValueFactory(new PropertyValueFactory<>("municipal"));

        postalCategoryColoumn.setMinWidth(100);
        postalCategoryColoumn.setCellValueFactory(new PropertyValueFactory<>("postalCategory"));

        // Populate the TableView by data from the patient register
        this.oblist =
                FXCollections.observableArrayList(this.postalCodeRegister.getPostalList());
        this.postalCodeTableView.setItems(this.oblist);
    }


    /**
     * Fills the register with dummy data.
     */
    private void fillRegisterWithDummyData() {
        this.postalCodeRegister.addPostalCode(new PostalCode("6900", "Florø", "4000", "Kinn", "G"));
        this.postalCodeRegister.addPostalCode(new PostalCode("4646", "Naustdal", "5000", "Sunnfjord", "F"));
        this.postalCodeRegister.addPostalCode(new PostalCode("7979", "Nordfjordeid", "8000", "Stadt","A"));
        this.postalCodeRegister.addPostalCode(new PostalCode("2815", "Gjøvik", "6000", "Gjøvik", "B"));
    }

    //from earlier version
    @FXML
    void readFileButton() {
        readFile();
    }

    /**
     * File being scanned, while more lines, divide each line in 5 parts, split with tabulator
     *  then put in PostalCode contructor and added to the ArrayList.
     *
     *  Every line is then put to the observable list which get them to the tableview.
     */
    void readFile(){
    try {
        File f = new File("postcodes.txt");
        Scanner sc = new Scanner(f);
        this.postalCodeRegister = new PostalCodeRegister();

        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String[] details = line.split("\t");
            String postalCode = details[0];
            String city = details[1];
            String municipalCode = details[2];
            String municipal = details[3];
            String postalCategory = details[4];

            PostalCode pcode = new PostalCode(postalCode, city, municipalCode, municipal, postalCategory);
            postalCodeRegister.addPostalCode(pcode);
        }

        this.oblist =
                FXCollections.observableArrayList(this.postalCodeRegister.getPostalList());
        this.postalCodeTableView.setItems(this.oblist);

    } catch (FileNotFoundException e) {
        e.printStackTrace();
        System.out.println("something went wrong");
    }
    }

    public void setSearchOblistToTableView(){

        //Arraylist, that populate the tableview
        //Clears previous search
        searchOblist.clear();
        //Pseudo code for retrieving searched textfield
       // textFieldWithListener(searchOblist.getList);
        //Sets values to tableview
        // Finalize the setup of the TableView
        postalCodeColoumn.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        cityColoumn.setCellValueFactory(new PropertyValueFactory<>("city"));
        municipalCodeColoumn.setCellValueFactory(new PropertyValueFactory<>("municipalCode"));
        municipalColoumn.setCellValueFactory(new PropertyValueFactory<>("municipal"));
        postalCategoryColoumn.setCellValueFactory(new PropertyValueFactory<>("postalCategory"));
        postalCodeTableView.setItems(searchOblist);
    }


    /**
     * Displays alert, an informative dialog about the tableview sorting and scrolling.
     */
    @FXML
    public void showAboutSortingDialog(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About sorting the table");
        alert.setHeaderText("Tips to find what you're looking for");
        alert.setContentText("By clicking on what you want to sort the table by\n"
                +" (Postal code, city, municipal, etc.)\n"
                +"▲ means from the start (lika A or 0001)\n"
                + "▼ means the bottom like (ÆØÅ or 9999)\n"
                + "     \nthen scroll away ;-)\n");

        alert.showAndWait();
    }

    /**
     * Displays alert, an informative dialog about the categories.
     */
    @FXML
    public void showAboutCategoriesDialog(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About post code categories");
        alert.setHeaderText("Post code categories, in Norweigian");
        alert.setContentText("Postnummerkategoriene forteller hva postnummeret blir benyttet til (f.eks. gateadresser og/eller postboksadresser).\n"
                +"B = Både gateadresser og postbokser\n"
                + "F = Flere bruksområder (felles)\n"
                + "G = Gateadresser (og stedsadresser), dvs. “grønne postkasser”\n"
                + "P = Postbokser\n"
                + "S = Servicepostnummer (disse postnumrene er ikke i bruk til postadresser)\n");

        alert.showAndWait();
    }

    /**
     * Closing the application
     */
    @FXML
    private void closeButtonAction() {
        // get a handle to the stage
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

}

